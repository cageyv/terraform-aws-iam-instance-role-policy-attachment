# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2020-05-13)


### Features

* Support 'stage' and 'environment' label params ([605f41f](https://gitlab.com/guardianproject-ops/terraform-aws-iam-instance-role-policy-attachment/commit/605f41f391590374afb613ae744ab46451aabb90))
